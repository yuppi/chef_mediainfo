#
# Cookbook Name:: mediainfo
# Recipe:: default
#
# Copyright 2014, Suzuki-Yuki
#
# All rights reserved - Do Not Redistribute
#
#--------------------------------------------------------------------
# mediainfoのインストール
#--------------------------------------------------------------------
package "mediainfo" do
    action :install
    options "--enablerepo=epel --enablerepo=remi"
end